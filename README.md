# About this

This is a set of Kubernetes manifests to deploy the webmail client [Rainloop](http://www.rainloop.net/). Rainloop is a simple, modern & fast web-based client.
The docker image used here is the one made by [youtous](https://gitlab.com/youtous/rainloop).

Note that for a production deployment using Ingress, you need to modify the nginx config found on the repository and add to the server config section, the line `server_name yourdomain.com`

# File Structure

```
.
├── rainloopdb-configmap.yaml
├── rainloopdb-deploy.yaml
├── rainloopdb-secret.yaml
└── rainloop-deploy.yaml
```



First we have the manifest for the deployment of the app itself. On this manifest we have four Kubernetes objects, the PersistenVolumeClaim to store the configuration data needed, the Deployment using the image mentioned before that listen on port 8888.
The Service and the Ingress object using Traefik.

Moving to the deployment of the database, the object used is a StatefulSet. On this case we are using a PostGreSQL database. The PVC is created to store the data used by RainLoop, which is only needed for saved contacts.
It used ConfigMaps and a Secret for the variable enviroment needed by Postgres.
